﻿using System;
using System.Drawing;

namespace AutoCookies {
    partial class AnimationForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ethernetIPforSLCMicroCom1 = new AdvancedHMIDrivers.EthernetIPforSLCMicroCom(this.components);
            this.SuspendLayout();
            // 
            // ethernetIPforSLCMicroCom1
            // 
            this.ethernetIPforSLCMicroCom1.CIPConnectionSize = 508;
            this.ethernetIPforSLCMicroCom1.DisableSubscriptions = false;
            this.ethernetIPforSLCMicroCom1.IPAddress = "192.168.0.10";
            this.ethernetIPforSLCMicroCom1.PollRateOverride = 500;
            this.ethernetIPforSLCMicroCom1.Port = 44818;
            this.ethernetIPforSLCMicroCom1.SynchronizingObject = this;
            // 
            // AnimationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 341);
            this.Name = "AnimationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Animation";
            this.Load += new System.EventHandler(this.loadAnimationForm);
            this.ResumeLayout(false);

        }        

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private AdvancedHMIDrivers.EthernetIPforSLCMicroCom ethernetIPforSLCMicroCom1;
    }
}