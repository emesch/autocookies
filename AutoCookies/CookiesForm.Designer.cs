﻿using AutoCookies.Model;
using System.Windows.Forms;

namespace AutoCookies {
    partial class CookiesForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.comboPortSelection = new System.Windows.Forms.ComboBox();
            this.ethernetIPforSLCMicroCom1 = new AdvancedHMIDrivers.EthernetIPforSLCMicroCom(this.components);
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.panelConnection = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.bStartManufactory = new AdvancedHMIControls.MomentaryButton();
            this.bStopManufactory = new AdvancedHMIControls.MomentaryButton();
            this.ConsoleLogBox = new System.Windows.Forms.TextBox();
            this.bStartForModbus = new AdvancedHMIControls.MomentaryButton();
            this.bStopForModbus = new AdvancedHMIControls.MomentaryButton();
            this.comboBaudSelection = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(3, 59);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(88, 23);
            this.buttonConnect.TabIndex = 0;
            this.buttonConnect.Text = "CONNECT";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.ButtonConnectClick);
            // 
            // comboPortSelection
            // 
            this.comboPortSelection.FormattingEnabled = true;
            this.comboPortSelection.Location = new System.Drawing.Point(38, 3);
            this.comboPortSelection.Name = "comboPortSelection";
            this.comboPortSelection.Size = new System.Drawing.Size(138, 21);
            this.comboPortSelection.TabIndex = 1;
            // 
            // ethernetIPforSLCMicroCom1
            // 
            this.ethernetIPforSLCMicroCom1.CIPConnectionSize = 508;
            this.ethernetIPforSLCMicroCom1.DisableSubscriptions = false;
            this.ethernetIPforSLCMicroCom1.IPAddress = "192.168.0.10";
            this.ethernetIPforSLCMicroCom1.PollRateOverride = 500;
            this.ethernetIPforSLCMicroCom1.Port = 44818;
            this.ethernetIPforSLCMicroCom1.SynchronizingObject = this;
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.Location = new System.Drawing.Point(88, 59);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(88, 23);
            this.buttonDisconnect.TabIndex = 5;
            this.buttonDisconnect.Text = "DISCONNECT";
            this.buttonDisconnect.UseVisualStyleBackColor = true;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnectClick);
            // 
            // panelConnection
            // 
            this.panelConnection.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panelConnection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelConnection.Controls.Add(this.comboBaudSelection);
            this.panelConnection.Controls.Add(this.label2);
            this.panelConnection.Controls.Add(this.label1);
            this.panelConnection.Controls.Add(this.buttonConnect);
            this.panelConnection.Controls.Add(this.buttonDisconnect);
            this.panelConnection.Controls.Add(this.comboPortSelection);
            this.panelConnection.Location = new System.Drawing.Point(12, 12);
            this.panelConnection.Name = "panelConnection";
            this.panelConnection.Size = new System.Drawing.Size(181, 90);
            this.panelConnection.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Port:";
            // 
            // bStartManufactory
            // 
            this.bStartManufactory.ButtonColor = MfgControl.AdvancedHMI.Controls.PushButton.ButtonColors.Green;
            this.bStartManufactory.CommComponent = this.ethernetIPforSLCMicroCom1;
            this.bStartManufactory.LegendPlate = MfgControl.AdvancedHMI.Controls.PushButton.LegendPlates.Large;
            this.bStartManufactory.Location = new System.Drawing.Point(29, 143);
            this.bStartManufactory.Name = "bStartManufactory";
            this.bStartManufactory.OutputType = MfgControl.AdvancedHMI.Controls.PushButton.OutputTypes.MomentarySet;
            this.bStartManufactory.PLCAddressClick = "";
            this.bStartManufactory.PLCAddressVisible = "";
            this.bStartManufactory.Size = new System.Drawing.Size(58, 85);
            this.bStartManufactory.TabIndex = 11;
            this.bStartManufactory.Text = "Start Demo";
            this.bStartManufactory.Click += new System.EventHandler(this.bStartManufactoryClick);
            // 
            // bStopManufactory
            // 
            this.bStopManufactory.ButtonColor = MfgControl.AdvancedHMI.Controls.PushButton.ButtonColors.Red;
            this.bStopManufactory.CommComponent = this.ethernetIPforSLCMicroCom1;
            this.bStopManufactory.LegendPlate = MfgControl.AdvancedHMI.Controls.PushButton.LegendPlates.Large;
            this.bStopManufactory.Location = new System.Drawing.Point(93, 143);
            this.bStopManufactory.Name = "bStopManufactory";
            this.bStopManufactory.OutputType = MfgControl.AdvancedHMI.Controls.PushButton.OutputTypes.MomentarySet;
            this.bStopManufactory.PLCAddressClick = "";
            this.bStopManufactory.PLCAddressVisible = "";
            this.bStopManufactory.Size = new System.Drawing.Size(58, 85);
            this.bStopManufactory.TabIndex = 11;
            this.bStopManufactory.Text = "Stop Demo";
            this.bStopManufactory.Click += new System.EventHandler(this.bStopManufactoryClick);
            // 
            // ConsoleLogBox
            // 
            this.ConsoleLogBox.Location = new System.Drawing.Point(12, 482);
            this.ConsoleLogBox.Multiline = true;
            this.ConsoleLogBox.Name = "ConsoleLogBox";
            this.ConsoleLogBox.Size = new System.Drawing.Size(703, 267);
            this.ConsoleLogBox.TabIndex = 12;
            // 
            // bStartForModbus
            // 
            this.bStartForModbus.ButtonColor = MfgControl.AdvancedHMI.Controls.PushButton.ButtonColors.Green;
            this.bStartForModbus.CommComponent = this.ethernetIPforSLCMicroCom1;
            this.bStartForModbus.LegendPlate = MfgControl.AdvancedHMI.Controls.PushButton.LegendPlates.Large;
            this.bStartForModbus.Location = new System.Drawing.Point(12, 234);
            this.bStartForModbus.Name = "bStartForModbus";
            this.bStartForModbus.OutputType = MfgControl.AdvancedHMI.Controls.PushButton.OutputTypes.MomentarySet;
            this.bStartForModbus.PLCAddressClick = "";
            this.bStartForModbus.PLCAddressVisible = "";
            this.bStartForModbus.Size = new System.Drawing.Size(75, 110);
            this.bStartForModbus.TabIndex = 11;
            this.bStartForModbus.Text = "Start for Modbus";
            this.bStartForModbus.Click += new System.EventHandler(this.bStartForModbusClick);
            // 
            // bStopForModbus
            // 
            this.bStopForModbus.ButtonColor = MfgControl.AdvancedHMI.Controls.PushButton.ButtonColors.Red;
            this.bStopForModbus.CommComponent = this.ethernetIPforSLCMicroCom1;
            this.bStopForModbus.LegendPlate = MfgControl.AdvancedHMI.Controls.PushButton.LegendPlates.Large;
            this.bStopForModbus.Location = new System.Drawing.Point(93, 234);
            this.bStopForModbus.Name = "bStopForModbus";
            this.bStopForModbus.OutputType = MfgControl.AdvancedHMI.Controls.PushButton.OutputTypes.MomentarySet;
            this.bStopForModbus.PLCAddressClick = "";
            this.bStopForModbus.PLCAddressVisible = "";
            this.bStopForModbus.Size = new System.Drawing.Size(75, 110);
            this.bStopForModbus.TabIndex = 11;
            this.bStopForModbus.Text = "Stop for Modbus";
            this.bStopForModbus.Click += new System.EventHandler(this.bStopForModbusClick);
            // 
            // comboBaudSelection
            // 
            this.comboBaudSelection.FormattingEnabled = true;
            this.comboBaudSelection.Items.AddRange(new object[] {
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400"});
            this.comboBaudSelection.Location = new System.Drawing.Point(38, 31);
            this.comboBaudSelection.Name = "comboBaudSelection";
            this.comboBaudSelection.Size = new System.Drawing.Size(138, 21);
            this.comboBaudSelection.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Baud:";
            // 
            // CookiesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 741);
            this.Controls.Add(this.ConsoleLogBox);
            this.Controls.Add(this.bStopForModbus);
            this.Controls.Add(this.bStopManufactory);
            this.Controls.Add(this.bStartForModbus);
            this.Controls.Add(this.bStartManufactory);
            this.Controls.Add(this.panelConnection);
            this.Name = "CookiesForm";
            this.Text = "AutoCookies";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.loadCookiesForm);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CookiesForm_Paint);
            this.panelConnection.ResumeLayout(false);
            this.panelConnection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonConnect;        
        private System.Windows.Forms.ComboBox comboPortSelection;
        private AdvancedHMIDrivers.EthernetIPforSLCMicroCom ethernetIPforSLCMicroCom1;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Panel panelConnection;
        private System.Windows.Forms.Label label1;
        private AdvancedHMIControls.MomentaryButton bStartManufactory;
        private AdvancedHMIControls.MomentaryButton bStopManufactory;
        private System.Windows.Forms.TextBox ConsoleLogBox;
        private AdvancedHMIControls.MomentaryButton bStopForModbus;
        private AdvancedHMIControls.MomentaryButton bStartForModbus;
        private ComboBox comboBaudSelection;
        private Label label2;
    }
}

