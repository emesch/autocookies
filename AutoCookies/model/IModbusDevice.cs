﻿using System;

namespace AutoCookies.Model {

    public interface IModbusDevice {

        void Start();
        void Stop();
        void Activate();
        void Deactivate();

    }
}