﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Timers;
using System.Threading;
using System.Collections.Generic;

namespace AutoCookies.Model {

    public abstract class Animation : Control, ILogger {

        protected static int SHAPE_COUNT = 0;

        #region Constructor
        public Animation(string text, int positionX, int positionY) : base(text) {
            this.lastFrameId = 0;
            this.currentFrameId = 0;
            this.Location = new Point(positionX, positionY);            

            this.state = AnimationState.STOPPED;
            this.speed = new AnimationSpeed(this, DefaultAnimationStartSpeed(), DefaultAnimationMaxSpeed(), true);

            this.repeat = true;
            this.backwards = false;            

            //InitializeTimer();
            InitializeFrames();
            LoadFrames();

            this.Size = CurrentFrame().Size;
            this.BackgroundImage = CurrentFrame();            

            this.DoubleBuffered = true;
        }
        #endregion

        #region Public defaults
        //public static readonly int DEFAULT_TIMER_INTERVAL = 100;

        public virtual double DefaultAnimationStartSpeed() {
            return 10;
        }

        public virtual double DefaultAnimationMaxSpeed() {
            return 25;
        }
        #endregion               

        #region Public getters
        public AnimationState GetState() {
            return this.state;
        }

        public int CountFrames() {
            return this.frames.Length;
        }

        public Image CurrentFrame() {
            return frames[currentFrameId];
        }

        public double GetCurrentSpeed() {
            return this.speed.GetCurrentSpeed();
        }

        public double GetMaxSpeed() {
            return this.speed.GetMaxSpeed();
        }

        /*
        public double GetTimerInterval() {
            return this.timer.Interval;
        }
        */
        #endregion

        #region Public interface
        public virtual void Start() {
            StartAnimation();
        }

        public virtual void Start(double accelerationValue, double maxSpeed) {
            SetAcceleration(true, accelerationValue, maxSpeed);
            StartAnimation();
        }

        public virtual void Stop() {
            if (this.speed.IsAccelerated()) {
                ChangeState(AnimationState.STOPPING);
            } else {
                StopAnimation();
            }
        }

        public void NextFrame() {
            if (this.currentFrameId < CountFrames() - 1) {
                UpdateFrame(this.currentFrameId + 1);
            } else if (repeat) {
                ToFirstFrame();
            }
        }

        public void PreviousFrame() {
            if (this.currentFrameId > 0) {
                UpdateFrame(this.currentFrameId - 1);
            } else if (repeat) {
                ToLastFrame();
            }
        }        

        public void ChangeDirection() {
            this.backwards = !this.backwards;
        }
        #endregion

        #region Interface implementations
        string ILogger.GetLoggerName() {
            return "Animation";
        }
        #endregion

        #region Timer methods
        /*
        public void UpdateTickSpeed() {
            StopTimer();
            try {
                this.timer.Interval = this.speed.GetTimerIntervalForCurrentSpeed();
                StartTimer();
            } catch (InvalidOperationException e) {
                this.timer.Interval = DEFAULT_TIMER_INTERVAL;
                StopAnimation();
            }           
        }
                      
        private void InitializeTimer() {
            this.timer = new System.Timers.Timer();
            this.timer.Interval = DEFAULT_TIMER_INTERVAL;
            this.timer.Elapsed += new ElapsedEventHandler(DoAfterTick);
        }
                
        private void DoAfterTick(object sender, EventArgs e) {
            //need look
            ChangeAnimationFrame();
            UpdateAnimationSpeed();
        }        
        
        private void StartTimer() {
            this.timer.Start();
        }

        private void StopTimer() {
            this.timer.Stop();
        }
        */
        #endregion

        #region Thread methods
        private void StartThread() {
            this.thread = new Thread(Animate);
            this.thread.Start();
        }

        internal int GetCurrentFrameId() {
            return this.currentFrameId;
        }

        private void Animate() {
            while (!AnimationState.STOPPED.Equals(GetState())) {                
                ChangeAnimationFrame();
                UpdateAnimationSpeed();
                SuspendAnimationForAnotherTick();
            }
        }

        private void SuspendAnimationForAnotherTick() {            
            try {                
                Thread.Sleep(this.speed.GetSuspendTimeForCurrentSpeed());
            } catch (InvalidOperationException e) {
                StopAnimation();
            }
            
        }
        #endregion

        #region Private methods
        private void ChangeState(AnimationState state) {
            AnimationState oldState = this.state;
            this.state = state;
            OnStateChanged(new DeviceStateChangedEventArgs() { previousState = oldState.ToString(), newState = this.state.ToString() });
        }

        private void ToNextState() {
            switch (GetState()) {
                case AnimationState.STARTING:
                    if (this.speed.IsAccelerated()) {
                        ChangeState(AnimationState.ACCELERATING);
                        break;
                    }
                    ChangeState(AnimationState.RUNNING_CONSTANT_SPEED);
                    break;
                case AnimationState.ACCELERATING:
                    if(GetCurrentSpeed() == GetMaxSpeed()) {
                        ChangeState(AnimationState.RUNNING_CONSTANT_SPEED);
                    }
                    break;
            }
        }        

        private void InitializeFrames() {
            int expectedFrames = ExpectedFrameCount();
            this.frames = new Bitmap[expectedFrames];
        }

        private void ToFirstFrame() {
            UpdateFrame(0);
        }

        private void ToLastFrame() {
            UpdateFrame(CountFrames() - 1);
        }

        private void UpdateFrame(int frameId) {            
            this.lastFrameId = this.currentFrameId;
            this.currentFrameId = frameId;
            this.BackgroundImage = CurrentFrame();
            OnFrameChanged();
            OnAnimationEnded();
        }        

        private void ChangeAnimationFrame() {
            if (!backwards) {
                NextFrame();
            } else {
                PreviousFrame();
            }
        }        

        private void UpdateAnimationSpeed() {
            this.speed.Update();
        }

        public void SetAcceleration(bool accelerated, double accelerationValue, double maxSpeed) {
            this.speed.SetAcceleration(accelerated, accelerationValue, maxSpeed);
        }

        private void StartAnimation() {
            ChangeState(AnimationState.STARTING);
            //StartTimer();
            StartThread();
        }       

        private void StopAnimation() {
            //StopTimer();            
            ChangeState(AnimationState.STOPPED);
        }        
        #endregion

        #region OnEvents methods
        public void OnCurrentSpeedChanged() {            
            if (currentSpeedChanged != null) {
                currentSpeedChanged(this, EventArgs.Empty);
                ToNextState();
            }
        }

        protected virtual void OnFrameChanged() {
            if (frameChanged != null) {
                frameChanged(this, new FrameChangedEventArgs() { lastFrameId = this.lastFrameId, currentFrameId = this.currentFrameId });
            }
        }

        protected virtual void OnAnimationEnded() {
            if (animationEnded != null) {
                animationEnded(this, EventArgs.Empty);
            }
        }

        protected virtual void OnStateChanged(DeviceStateChangedEventArgs e) {
            if (stateChanged != null) {
                stateChanged(this, e);
            }
        }
        #endregion        

         

        public delegate void FrameChangedEventHandler(object source, FrameChangedEventArgs args);
        public event FrameChangedEventHandler frameChanged;
        public delegate void AnimationEndedEventHandler(object source, EventArgs args);
        public event AnimationEndedEventHandler animationEnded;
        public delegate void AnimationStateChangedEventHandler(object source, DeviceStateChangedEventArgs args);
        public event AnimationStateChangedEventHandler stateChanged;
        public delegate void AnimationSpeedChangedEventHandler(object source, EventArgs args);
        public event AnimationSpeedChangedEventHandler currentSpeedChanged;

        protected abstract int ExpectedFrameCount();
        protected abstract void LoadFrames();        

        protected Bitmap[] frames;
        protected int lastFrameId;
        protected int currentFrameId;

        //private System.Timers.Timer timer;
        private Thread thread;
        private AnimationState state;
        private AnimationSpeed speed;

        protected bool repeat;
        private bool backwards;

    }    

    public static class AnimationFactory {

        public static Animation createAnimation(string text, Animations animationType, int positionX, int positionY) {
            Animation newAnimation;
            switch (animationType) {
                case Animations.PRODUCTION_LINE:
                    newAnimation = new ProductionLineAnimation(text, positionX, positionY);
                    break;
                case Animations.SHAPE_GENERATOR:
                    newAnimation = new ShapeGeneratorAnimation(text, positionX, positionY);
                    break;
                case Animations.PRODUCTION_LINE_SENSOR_LIGHT:
                    newAnimation = new ProductionLineSensorLightAnimation(text, positionX, positionY);
                    break;
                case Animations.COOKIE:
                    newAnimation = new CookieAnimation(text, positionX, positionY);
                    break;
                case Animations.XYZ_TOOL:
                    newAnimation = new ToolAnimation(text, positionX, positionY);                    
                    break;
                case Animations.VERTICAL_PRODUCTION_LINE:
                    newAnimation = new VerticalProductionLineAnimation(text, positionX, positionY);
                    break;
                case Animations.TRAY:
                    newAnimation = new TrayAnimation(text, positionX, positionY);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("deviceType", "Can't create device. Unknown deviceType.");
            }
            return newAnimation;
        }

    }

    public enum Animations {
        PRODUCTION_LINE, SHAPE_GENERATOR, PRODUCTION_LINE_SENSOR_LIGHT, COOKIE, XYZ_TOOL, COOKIE_GRABBER,
        VERTICAL_PRODUCTION_LINE, TRAY
    }

    class ProductionLineAnimation : Animation {

        internal ProductionLineAnimation(string text, int positionX, int positionY) : base(text, positionX, positionY) {

        }        

        #region Overriden methods
        protected override int ExpectedFrameCount() {
            return 25;
        }

        protected override void LoadFrames() {
            frames[0] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_1);
            frames[1] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_2);
            frames[2] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_3);
            frames[3] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_4);
            frames[4] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_5);
            frames[5] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_6);
            frames[6] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_7);
            frames[7] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_8);
            frames[8] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_9);
            frames[9] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_10);
            frames[10] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_11);
            frames[11] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_12);
            frames[12] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_13);
            frames[13] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_14);
            frames[14] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_15);
            frames[15] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_16);
            frames[16] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_17);
            frames[17] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_18);
            frames[18] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_19);
            frames[19] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_20);
            frames[20] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_21);
            frames[21] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_22);
            frames[22] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_23);
            frames[23] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_24);
            frames[24] = new Bitmap(AutoCookies.Properties.Resources.horizontalProductionLine1_25);        }
        #endregion


    }

    class VerticalProductionLineAnimation : Animation {

        internal VerticalProductionLineAnimation(string text, int positionX, int positionY) : base(text, positionX, positionY) {

        }

        #region Overriden methods
        protected override int ExpectedFrameCount() {
            return 25;
        }

        protected override void LoadFrames() {
            frames[0] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_1);
            frames[1] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_2);
            frames[2] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_3);
            frames[3] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_4);
            frames[4] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_5);
            frames[5] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_6);
            frames[6] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_7);
            frames[7] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_8);
            frames[8] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_9);
            frames[9] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_10);
            frames[10] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_11);
            frames[11] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_12);
            frames[12] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_13);
            frames[13] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_14);
            frames[14] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_15);
            frames[15] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_16);
            frames[16] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_17);
            frames[17] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_18);
            frames[18] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_19);
            frames[19] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_20);
            frames[20] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_21);
            frames[21] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_22);
            frames[22] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_23);
            frames[23] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_24);
            frames[24] = new Bitmap(AutoCookies.Properties.Resources.verticalProductionLine1_25);
        }
        #endregion

    }

    class ShapeGeneratorAnimation : Animation {

        #region Constructor
        public ShapeGeneratorAnimation(string text, int positionX, int positionY) : base(text, positionX, positionY) {
            this.state = ColorState.OFF;
        }
        #endregion

        #region Public properties
        public ColorState State {
            get {
                return this.state;                
            }
            set {
                this.state = value;
                UpdateFrame();
            }
        }
        #endregion

        public void UpdateFrame() {
            this.lastFrameId = this.currentFrameId;
            switch (this.state) {
                case ColorState.OFF:
                    this.currentFrameId = 0;                    
                    break;
                case ColorState.RED:
                    this.currentFrameId = 1;
                    break;
                case ColorState.YELLOW:
                    this.currentFrameId = 2;
                    break;
                case ColorState.GREEN:
                    this.currentFrameId = 3;
                    break;
            }
            this.BackgroundImage = CurrentFrame();
        }

        #region Overriden base methods
        public override void Start() {
            
        }

        public override void Start(double accelerationValue, double maxSpeed) {
            
        }

        public override void Stop() {
            
        }
        #endregion

        #region Loading bitmaps
        protected override int ExpectedFrameCount() {
            return 4;
        }

        protected override void LoadFrames() {
            frames[0] = new Bitmap(AutoCookies.Properties.Resources.shapeGenerator_off);
            frames[1] = new Bitmap(AutoCookies.Properties.Resources.shapeGenerator_red);
            frames[2] = new Bitmap(AutoCookies.Properties.Resources.shapeGenerator_yellow);
            frames[3] = new Bitmap(AutoCookies.Properties.Resources.shapeGenerator_green);
        }
        #endregion

        private ColorState state;

    }

    class ProductionLineSensorLightAnimation : Animation {

        #region Constructor
        internal ProductionLineSensorLightAnimation(string text, int positionX, int positionY) : base(text, positionX, positionY) {
            this.state = ColorState.OFF;                                                                              
        }        
        #endregion

        #region Public properties
        public ColorState State {
            get {
                return this.state;
            }
            set {
                this.state = value;                
                UpdateFrame();                
            }
        }
        #endregion

        public void UpdateFrame() {
            this.lastFrameId = this.currentFrameId;
            switch (this.state) {
                case ColorState.OFF:
                    this.currentFrameId = 0;
                    break;                
                case ColorState.YELLOW:
                    this.currentFrameId = 1;
                    break;
                case ColorState.GREEN:
                    this.currentFrameId = 2;
                    break;
            }
            this.BackgroundImage = CurrentFrame();
        }

        #region Loading bitmaps
        protected override int ExpectedFrameCount() {
            return 3;
        }

        protected override void LoadFrames() {
            frames[0] = new Bitmap(AutoCookies.Properties.Resources.light1_off);
            frames[1] = new Bitmap(AutoCookies.Properties.Resources.light1_yellow);
            frames[2] = new Bitmap(AutoCookies.Properties.Resources.light1_green);
        }
        #endregion

        private ColorState state;
    }

    class ToolAnimation : Animation {

        #region Constructor
        internal ToolAnimation(string text, int positionX, int positionY) : base(text, positionX, positionY) {
            this.repeat = false;
        }
        #endregion

        #region Public properties
        public CookieFillerState State {
            get {
                return this.state;
            }
        }
        #endregion

        protected override int ExpectedFrameCount() {
            return 7;
        }

        protected override void LoadFrames() {
            frames[0] = new Bitmap(AutoCookies.Properties.Resources.cookieFiller_1);
            frames[1] = new Bitmap(AutoCookies.Properties.Resources.cookieFiller_2);
            frames[2] = new Bitmap(AutoCookies.Properties.Resources.cookieFiller_3);
            frames[3] = new Bitmap(AutoCookies.Properties.Resources.cookieFiller_4);
            frames[4] = new Bitmap(AutoCookies.Properties.Resources.cookieFiller_5);
            frames[5] = new Bitmap(AutoCookies.Properties.Resources.cookieFiller_6);
            frames[6] = new Bitmap(AutoCookies.Properties.Resources.cookieFiller_7);
        }

        private CookieFillerState state;

    }

    class CookieAnimation : Animation {        

        #region Constructor
        public CookieAnimation(string text, int positionX, int positionY) : base(text, positionX, positionY) {
            this.id = SHAPE_COUNT;
            SHAPE_COUNT++;
        }
        #endregion        

        #region Public interface
        public void Fill() {
            NextFrame();
            if(GetCurrentFrameId() == 6) {
                this.filled = true;
            }
        }

        public bool IsFilled() {
            return this.filled;
        }
        #endregion

        #region Loading bitmaps
        protected override int ExpectedFrameCount() {
            return 8;
        }

        protected override void LoadFrames() {
            frames[0] = new Bitmap(AutoCookies.Properties.Resources.bigCookie1_1);
            frames[1] = new Bitmap(AutoCookies.Properties.Resources.bigCookie1_2);
            frames[2] = new Bitmap(AutoCookies.Properties.Resources.bigCookie1_3);
            frames[3] = new Bitmap(AutoCookies.Properties.Resources.bigCookie1_4);
            frames[4] = new Bitmap(AutoCookies.Properties.Resources.bigCookie1_5);
            frames[5] = new Bitmap(AutoCookies.Properties.Resources.bigCookie1_6);
            frames[6] = new Bitmap(AutoCookies.Properties.Resources.bigCookie1_7);
            frames[7] = new Bitmap(AutoCookies.Properties.Resources.bigCookie1_8);
        }
        #endregion

        public int id { get; private set; }        

        private bool filled;

    }

    class TrayAnimation : Animation {        

        #region Constructor
        public TrayAnimation(string text, int positionX, int positionY) : base(text, positionX, positionY) {
            this.id = SHAPE_COUNT;
            SHAPE_COUNT++;

            this.cookies = new Stack<CookieAnimation>();
        }
        #endregion

        #region Public interface
        internal Point GetFirstFreeSpace() {
            Point firstFreeSpace = new Point(Location.X + 10, Location.Y + 5);
            int nOfCookies = cookies.Count;
            if(nOfCookies > 0 && nOfCookies <= 4) {
                firstFreeSpace = new Point(firstFreeSpace.X + nOfCookies * 30, firstFreeSpace.Y);
            }else if(nOfCookies > 4 && nOfCookies <= 9) {
                firstFreeSpace = new Point(firstFreeSpace.X + (nOfCookies - 5) * 30, firstFreeSpace.Y + 30);
            } else if (nOfCookies > 9 && nOfCookies <= 14) {
                firstFreeSpace = new Point(firstFreeSpace.X + (nOfCookies - 10) * 30, firstFreeSpace.Y + 60);
            } else if (nOfCookies > 14 && nOfCookies <= 19) {
                firstFreeSpace = new Point(firstFreeSpace.X + (nOfCookies - 15) * 30, firstFreeSpace.Y + 90);
            }             

            return firstFreeSpace;
        }

        internal void AddCookie(CookieAnimation cookie) {
            cookies.Push(cookie);
        }
        #endregion        

        #region Loading bitmaps
        protected override int ExpectedFrameCount() {
            return 1;
        }

        protected override void LoadFrames() {
            frames[0] = new Bitmap(AutoCookies.Properties.Resources.tray1);
        }
        #endregion

        public int id { get; private set; }
        private Stack<CookieAnimation> cookies;

        internal bool isFull() {
            return cookies.Count >= 20; 
        }
    }

    public class FrameChangedEventArgs : EventArgs {
        public int currentFrameId;
        public int lastFrameId;               
    } 

}
