﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace AutoCookies.Model {    

    public abstract class Device : ILogger{        

        #region Constructor
        protected Device(int id, string name, Component guiComponent) {
            this.id = id;
            this.name = name;
            this.guiComponent = guiComponent;            

            if (this is IAnimated) {
                ((IAnimated)this).GetAnimation().frameChanged += ((IAnimated)this).OnFrameChanged;
                ((IAnimated)this).GetAnimation().animationEnded += ((IAnimated)this).OnAnimationEnded;
                ((IAnimated)this).GetAnimation().stateChanged += ((IAnimated)this).OnAnimationStateChanged;
                ((IAnimated)this).GetAnimation().currentSpeedChanged += ((IAnimated)this).OnAnimationSpeedChanged;
            }            
        }
        #endregion

        #region Public properties
        public Manufactory Manufactory {
            get {
                return Program.manufactory;
            }
        }
        #endregion

        #region Public getters
        public string GetLoggerName() {
            return "Device (" + this.id + ", " + this.name + ")";
        }
        #endregion

        #region OnEventMethods
        public void OnAnimationStateChanged(object source, DeviceStateChangedEventArgs e) {
            if (deviceStateChanged != null) {
                deviceStateChanged(this, new DeviceStateChangedEventArgs() { previousState = e.previousState, newState = e.newState});
            }
        }

        public void OnDeviceMessage(object source, DeviceMessageEventArgs e) {
            if (deviceMessage != null) {
                deviceMessage(this, new DeviceMessageEventArgs() { message = e.message });
            }
        }

        public abstract void OnManufactoryMessage(object source, ManufactoryMessageEventArgs args);
        #endregion        

        public delegate void DeviceStateChangedEventHandler(object source, DeviceStateChangedEventArgs args);
        public event DeviceStateChangedEventHandler deviceStateChanged;
        public delegate void DeviceMessageEventHandler(object source, DeviceMessageEventArgs args);
        public event DeviceMessageEventHandler deviceMessage;

        public int id { get; protected set; }
        public string name { get; protected set; }

        public abstract void StartDemo();
        public abstract void StopDemo();
        public abstract string GetState();       

        protected Component guiComponent;       
        
    }

    static class DeviceFactory {

        public static Device createDevice(int id, string name, Devices deviceType, Component guiComponent) {
            Device newDevice;
            switch (deviceType) {
                case Devices.PRODUCTION_LINE:
                    newDevice = new ProductionLine(id, name, guiComponent);
                    break;
                case Devices.SHAPE_GENERATOR:
                    newDevice = new ShapeGenerator(id, name, guiComponent);
                    break;
                case Devices.PRODUCTION_LINE_SENSOR:
                    newDevice = new ProductionLineSensor(id, name, guiComponent);
                    break;
                case Devices.COOKIE_FILLER:
                    newDevice = new CookieFiller(id, name, guiComponent);
                    break;
                case Devices.COOKIE_GRABBER:
                    newDevice = new CookieGrabber(id, name, guiComponent);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("deviceType", "Can't create device. Unknown deviceType.");
            }
            return newDevice;
        }

    }

    enum Devices {
        PRODUCTION_LINE, SHAPE_GENERATOR, PRODUCTION_LINE_SENSOR, COOKIE_FILLER, COOKIE_GRABBER
    }

    class ProductionLine : Device, IAnimated, IModbusDevice {

        #region Constructor
        public ProductionLine(int id, string name, Component guiComponent) : base(id, name, guiComponent) {
            this.placement = 0;
            this.maxSpeed = 50;
        }
        #endregion

        #region Public properties
        public ProductionLineSensor Sensor {
            get {
                return this.sensor;
            }
            set {
                this.sensor = value;
                value.ProductionLine = this;
            }
        }

        public ShapeGenerator ShapeGenerator {
            get {
                return this.shapeGenerator;
            }
            set {
                this.shapeGenerator = value;
                value.ProductionLine = this;
            }
        }

        public CookieFiller CookieFiller {
            get {
                return this.cookieFiller;
            }
            set {
                this.cookieFiller = value;
            }
        }

        public CookieGrabber CookieGrabber {
            get {
                return this.cookieGrabber;
            }
            set {
                this.cookieGrabber = value;
            }
        }

        public bool Waiting {
            get {
                return this.isWaiting;
            }
            set {
                this.isWaiting = value;
            }
             
        }

        public ushort MaxSpeed {
            get {
                return (ushort)this.maxSpeed;
            }
            set {
                this.maxSpeed = value;
                ((ProductionLineAnimation)GetAnimation()).SetAcceleration(true, 1.8, this.maxSpeed);
            }
        }
        #endregion

        #region Public getters
        public Animation GetAnimation() {
            return (Animation)guiComponent;
        }

        public bool IsReadyToMove() {
            return this.allShapesGenerated && (this.cookiesFilled && cookiesGrabbed || isProductionLine4() && !Waiting);  
        }

        internal int GetCurrentPlacement() {
            return this.placement;
        }
        #endregion

        #region Overriden public Device methods
        public override void StartDemo() {
            Sensor.UpdateAnimationState(ColorState.OFF);
            ResetFlags();
            MoveForward();
        }        

        public override void StopDemo() {
            StopMoving();
        }

        public override string GetState() {
            return GetAnimation().GetState().ToString();
        }

        public override void OnManufactoryMessage(object source, ManufactoryMessageEventArgs args) {
            
        }
        #endregion

        #region IModbus implementation
        void IModbusDevice.Start() {            
            if (Sensor != null) {
                Sensor.UpdateAnimationState(ColorState.OFF);
            }            
            ResetFlags();
            
            Program.manufactory.UpdateHoldingRegister(id * 10, (ushort)this.maxSpeed);
            Program.manufactory.UpdateCoilRegister(id * 10, true);           
        }

        void IModbusDevice.Stop() {
            StopMoving();
            Program.manufactory.UpdateCoilRegister(id * 10, false);
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, false);
        }

        void IModbusDevice.Activate() {
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, true);
            MoveForward();            
        }

        void IModbusDevice.Deactivate() {
            StopMoving();
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, false);
        }
        #endregion

        #region Public interface
        public void MoveForward() {
            GetAnimation().Start(1.8, this.maxSpeed);
        }

        public void StopMoving() {
            GetAnimation().Stop();
        }

        public void AllShapesGenerated() {
            this.allShapesGenerated = true;
        }

        internal void CookiesInRangeFilled() {
            this.cookiesFilled = true;
        }

        internal void CookiesInRangeGrabbed() {
            this.cookiesGrabbed = true;
        }
        #endregion

        #region Private methods
        private void MoveObjectsOnProductionLine(FrameChangedEventArgs e) {
            ((CookiesForm)Program.mainWindow).GetAnimationForm().MoveAllObjectsOnPLine(id);
        }

        private void ResetFlags() {
            this.allShapesGenerated = false;
            this.cookiesFilled = false;
            this.cookiesGrabbed = false;
            this.isWaiting = false;
        }

        private bool isProductionLine4() {
            return this.id == (int)ManufactoryDevices.PRODUCTION_LINE4_ID;
        }
        #endregion

        #region OnEvent methods
        public void OnFrameChanged(object source, FrameChangedEventArgs e) {
            MoveObjectsOnProductionLine(e);
            if(this.placement >= 600) {
                this.placement = 0;
            }
            this.placement += 2;
            
            if (Sensor != null && IsCurrentPlacementInProductionPeriod() ) {
                Sensor.DetectProductionPeriod();
            }
            
        }

        private bool IsCurrentPlacementInProductionPeriod() {
            return GetCurrentPlacement() == Sensor.Placement
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 2
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 3
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 4
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 5
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 6
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 7
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 8
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 9
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 10
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 11
                || GetCurrentPlacement() == Sensor.Placement + Sensor.Interspace * 12;


        }

        public void OnAnimationSpeedChanged(object source, EventArgs args) {
            
        }

        public void OnAnimationEnded(object source, EventArgs e) {
            //GetAnimation().Invalidate();
            //GetAnimation().Update();
            //GetAnimation().Refresh();
        }        
        #endregion

        private ProductionLineSensor sensor;
        private ShapeGenerator shapeGenerator;
        private CookieFiller cookieFiller;
        private CookieGrabber cookieGrabber;

        private bool allShapesGenerated;
        private bool cookiesFilled;
        private bool cookiesGrabbed;
        private bool isWaiting; 

        private int placement;
        private double maxSpeed;
    }

    class ProductionLineSensor : Device, IModbusDevice {       

        #region Constructor
        public ProductionLineSensor(int id, string name, Component guiComponent) : base(id, name, guiComponent) {
            this.placement = 30;
            this.interspace = DefaultInterspace;
        }
        #endregion

        #region IModbus implementation
        void IModbusDevice.Start() {
            Program.manufactory.UpdateHoldingRegister(id * 10, (ushort)this.placement);
            Program.manufactory.UpdateHoldingRegister(id * 10 + 1, (ushort)this.interspace);
            
            Program.manufactory.UpdateCoilRegister(id * 10, true);
        }

        void IModbusDevice.Stop() {
            Program.manufactory.UpdateCoilRegister(id * 10, false);
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, false);
            UpdateAnimationState(ColorState.OFF);
        }

        void IModbusDevice.Activate() {
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, true);
            UpdateAnimationState(ColorState.YELLOW);
        }

        void IModbusDevice.Deactivate() {
            UpdateAnimationState(ColorState.OFF);
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, false);
        }
        #endregion

        #region Public interface
        public int Placement {
            get {
                return this.placement;
            }
            set {
                this.placement = value;
            }
        }

        public int Interspace {
            get {
                return this.interspace;
            }
            set {
                this.interspace = value;
            }
        }

        public ProductionLine ProductionLine {
            get {
                return this.productionLine;
            }
            set {
                this.productionLine = value;
            }
        }

        public int DefaultInterspace {
            get {
                return 50;
            }                         
        }
        #endregion

        #region Public overriden methods
        public override string GetState() {
            return GetAnimation().State.ToString();
        }

        public override void StartDemo() {

        }

        public override void StopDemo() {
            UpdateAnimationState(ColorState.OFF);
        }

        public override void OnManufactoryMessage(object source, ManufactoryMessageEventArgs args) {
            switch (args.message) {
                case ManufactoryMessage.PRODUCTION_LINE_STOPPED:
                    if (args.intValue == this.productionLine.id && ColorState.YELLOW.Equals(GetAnimation().State)) {
                        UpdateAnimationState(ColorState.GREEN);
                    }

                    break;
            }
        }
        #endregion

        #region Public interface
        public ProductionLineSensorLightAnimation GetAnimation() {
            return (ProductionLineSensorLightAnimation)this.guiComponent;
        }

        public void DetectProductionPeriod() {
            UpdateAnimationState(ColorState.YELLOW);
            OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.PRODUCTION_LINE_PERIOD_DETECTED });
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, true);
        }

        public void UpdateAnimationState(ColorState state) {
            String oldState = GetAnimation().State.ToString();
            GetAnimation().State = state;
            OnAnimationStateChanged(this, new DeviceStateChangedEventArgs() { log = true, previousState = oldState, newState = GetAnimation().State.ToString() });
        }        
        #endregion

        private int placement;
        private int interspace;
        private ProductionLine productionLine;        

    }

    class ShapeGenerator : Device, IModbusDevice {        

        #region Constructor
        public ShapeGenerator(int id, string name, Component guiComponent) : base(id, name, guiComponent) {
            this.shapeCount = 1;

            TypeOfShape = DefaultTypeOfShape;

            this.locations = new Dictionary<int, Point>();
        }
        #endregion

        #region IModbus implementation
        void IModbusDevice.Start() {
            Program.manufactory.UpdateHoldingRegister(id * 10, (ushort)locations.Values.Last().X);
            Program.manufactory.UpdateHoldingRegister(id * 10 + 1, (ushort)locations.Values.Last().Y);

            Program.manufactory.UpdateCoilRegister(id * 10, true);
            GetAnimation().State = ColorState.RED;
        }

        void IModbusDevice.Stop() {
            Program.manufactory.UpdateCoilRegister(id * 10, false);
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, false);
            GetAnimation().State = ColorState.OFF;
        }

        void IModbusDevice.Activate() {
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, true);
            GenerateShapes();
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, true);
        }

        void IModbusDevice.Deactivate() {
            Program.manufactory.UpdateCoilRegister(id * 10 + 1, false);
        }
        #endregion

        #region Public getters and Properties
        public ShapeGeneratorAnimation GetAnimation() {
            return (ShapeGeneratorAnimation)this.guiComponent;
        }

        public ProductionLine ProductionLine {
            get {
                return this.productionLine;
            }
            set {
                this.productionLine = value;                
            }
        }

        public int ShapeCount {
            get {
                return this.shapeCount;
            }
            set {
                this.shapeCount = value;
            }
        }

        public Animations TypeOfShape {
            get {
                return this.typeOfShape;
            }
            set {
                this.typeOfShape = value;
            }
        }

        public Animations DefaultTypeOfShape {
            get {
                return Animations.COOKIE;
            }
        }
        #endregion

        #region Overriden public Device methods
        public override string GetState() {            
            return GetAnimation().State.ToString();
        }

        public override void StartDemo() {
            GetAnimation().State = ColorState.RED;
        }

        public override void StopDemo() {
            GetAnimation().State = ColorState.OFF;
        }

        public override void OnManufactoryMessage(object source, ManufactoryMessageEventArgs args) {
            
        }
        #endregion

        #region Public interface
        public void AdjustGeneratedShapeLocation(int id, Point point) {
            if (!locations.ContainsKey(id)) {
                locations.Add(id, point);
            }                        
        }

        public void Activate() {
            Task.Factory.StartNew(() => {
                this.UpdateAnimationState(ColorState.YELLOW);
                GenerateShapes();
                this.UpdateAnimationState(ColorState.RED);
                OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.SHAPE_GENERATED });
            });
        }

        public void UpdateAnimationState(ColorState state) {
            GetAnimation().State = state;
        }
        #endregion

        #region Private methods
        private void GenerateShapes() {
            for (int i = 0; i < ShapeCount; i++) {
                Thread.Sleep(300);
                GenerateShape();
                Thread.Sleep(300);
                OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.ALL_SHAPES_GENERATED });
            }

        }

        private void GenerateShape() {
            this.UpdateAnimationState(ColorState.GREEN);
            
            ((CookiesForm)Program.mainWindow).GetAnimationForm().GenerateShape(TypeOfShape, ProductionLine.id, locations[0].X, locations[0].Y);

            Thread.Sleep(1000);
            this.UpdateAnimationState(ColorState.YELLOW);
            OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.SHAPE_GENERATED });
        }        

        private ProductionLine productionLine;
        private int shapeCount;
        Animations typeOfShape;

        private Dictionary<int, Point> locations;


    }

    abstract class ToolDevice : Device {

        #region Constructor
        public ToolDevice(int id, string name, Component guiComponent) : base(id, name, guiComponent) {
            IddlePosition = ((Control)guiComponent).Location;
        }
        #endregion

        public Point IddlePosition {
            get {
                return this.iddlePosition;
            }
            private set {
                this.iddlePosition = value;
            }
        }

        public ProductionLine ProductionLine {
            get {
                return this.productionLine;
            }
            set {
                this.productionLine = value;
                if(this is CookieFiller) {
                    value.CookieFiller = (CookieFiller)this;
                }else if(this is CookieGrabber) {
                    value.CookieGrabber = (CookieGrabber)this;
                }                
            }
        }

        public int MinXPosition {
            get {
                return IddlePosition.X - 50;
            }
        }

        public int MaxXPosition {
            get {
                return IddlePosition.X + 50;
            }
        }

        public override string GetState() {
            return GetAnimation().State.ToString();
        }

        internal ToolAnimation GetAnimation() {
            return (ToolAnimation)this.guiComponent;
        }

        public override void OnManufactoryMessage(object source, ManufactoryMessageEventArgs args) {

        }

        public override void StartDemo() {

        }

        public override void StopDemo() {
            OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.TOOL_DEVICE_STOPPED });
        }

        #region Private methods
        protected CookieAnimation[] SearchForShapesInRange() {
            return ((CookiesForm)Program.mainWindow).GetAnimationForm().GetCookiesInRange(ProductionLine.id, MinXPosition, MaxXPosition);
        }

        protected void GoToLocation(Point location) {
            ShiftY(location.Y);
            ShiftX(location.X);
        }

        public void ShiftX(int toXLocation) {
            Point currentLocation = GetAnimation().Location;
            if (currentLocation.X > toXLocation) {
                //GetAnimation().Location = new Point(currentLocation.X - 1, currentLocation.Y);
                ((CookiesForm)Program.mainWindow).GetAnimationForm().MoveAnimationElement(GetAnimation(), new Point(currentLocation.X - 1, currentLocation.Y));
                Thread.Sleep(5);
                ShiftX(toXLocation);
            } else if (currentLocation.X < toXLocation) {
                //GetAnimation().Location = new Point(currentLocation.X + 1, currentLocation.Y);
                ((CookiesForm)Program.mainWindow).GetAnimationForm().MoveAnimationElement(GetAnimation(), new Point(currentLocation.X + 1, currentLocation.Y));
                Thread.Sleep(5);
                ShiftX(toXLocation);
            }
        }

        public void ShiftY(int toYLocation) {
            Point currentLocation = GetAnimation().Location;
            if (currentLocation.Y > toYLocation) {
                ((CookiesForm)Program.mainWindow).GetAnimationForm().MoveAnimationElement(GetAnimation(), new Point(currentLocation.X, currentLocation.Y - 1));
                Thread.Sleep(10);
                ShiftY(toYLocation);
            } else if (currentLocation.Y < toYLocation) {
                ((CookiesForm)Program.mainWindow).GetAnimationForm().MoveAnimationElement(GetAnimation(), new Point(currentLocation.X, currentLocation.Y + 1));
                Thread.Sleep(10);
                ShiftY(toYLocation);
            }
        }

        public void ShiftX(int toXLocation, CookieAnimation withShape) {
            Point currentLocation = GetAnimation().Location;
            AnimationForm aForm = ((CookiesForm)Program.mainWindow).GetAnimationForm();
            if (currentLocation.X > toXLocation) {
                Point newLocation = new Point(currentLocation.X - 1, currentLocation.Y);
                aForm.MoveAnimationElement(GetAnimation(), newLocation);
                aForm.MoveAnimationElement(withShape, newLocation);
                Thread.Sleep(5);
                ShiftX(toXLocation, withShape);
            } else if (currentLocation.X < toXLocation) {
                Point newLocation = new Point(currentLocation.X + 1, currentLocation.Y);
                aForm.MoveAnimationElement(GetAnimation(), newLocation);
                aForm.MoveAnimationElement(withShape, newLocation);
                Thread.Sleep(5);
                ShiftX(toXLocation, withShape);
            }
        }

        public void ShiftY(int toYLocation, CookieAnimation withShape) {
            Point currentLocation = GetAnimation().Location;
            AnimationForm aForm = ((CookiesForm)Program.mainWindow).GetAnimationForm();
            if (currentLocation.Y > toYLocation) {
                Point newLocation = new Point(currentLocation.X, currentLocation.Y - 1);
                aForm.MoveAnimationElement(GetAnimation(), newLocation);
                aForm.MoveAnimationElement(withShape, newLocation);
                Thread.Sleep(10);
                ShiftY(toYLocation, withShape);
            } else if (currentLocation.Y < toYLocation) {
                Point newLocation = new Point(currentLocation.X, currentLocation.Y + 1);
                aForm.MoveAnimationElement(GetAnimation(), newLocation);
                aForm.MoveAnimationElement(withShape, newLocation);                
                Thread.Sleep(10);
                ShiftY(toYLocation, withShape);
            }
        }

        public void GoDown() {
            Animation animation = GetAnimation();
            while (animation.GetCurrentFrameId() < animation.CountFrames() - 1) {
                ((CookiesForm)Program.mainWindow).GetAnimationForm().GoDown(animation);
                Thread.Sleep(10);
            }
        }

        public void GoUp() {
            Animation animation = GetAnimation();
            while (animation.GetCurrentFrameId() > 0) {
                ((CookiesForm)Program.mainWindow).GetAnimationForm().GoUp(animation);
                Thread.Sleep(10);
            }
        }

        protected void ReturnToTheIddlePosition() {
            ShiftX(IddlePosition.X);
            ShiftY(IddlePosition.Y);
        }
        #endregion

        internal abstract void Activate();        

        private ProductionLine productionLine;
        private Point iddlePosition;
    }

    class CookieFiller : ToolDevice, IModbusDevice {

        #region Constructor
        public CookieFiller(int id, string name, Component guiComponent) : base(id, name, guiComponent) {
            
        }
        #endregion

        #region IModbus implementation
        void IModbusDevice.Start() {
            Program.manufactory.UpdateHoldingRegister(id * 10, (ushort)this.MinXPosition);
            Program.manufactory.UpdateHoldingRegister(id * 10 + 1, (ushort)this.MaxXPosition);

            Program.manufactory.UpdateCoilRegister(id * 10, true);
        }

        void IModbusDevice.Stop() {
            Program.manufactory.UpdateCoilRegister(id * 10, false);
            Program.manufactory.UpdateCoilRegister(id + 1, false);
        }

        void IModbusDevice.Activate() {
            Program.manufactory.UpdateCoilRegister(id + 1, true);
            Task.Factory.StartNew(() => {
                CookieAnimation[] shapesInRange = SearchForShapesInRange();
                if (shapesInRange.Length > 0) {
                    foreach (CookieAnimation shape in shapesInRange) {
                        if (!shape.IsFilled()) {    
                            if(id == (int)ManufactoryDevices.COOKIE_FILLER1_ID) {
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER1_SHAPE_LOCATION_X, (ushort)shape.Location.X);
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER1_SHAPE_LOCATION_Y, (ushort)shape.Location.Y);
                            }else if(id == (int)ManufactoryDevices.COOKIE_FILLER2_ID) {
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER2_SHAPE_LOCATION_X, (ushort)shape.Location.X);
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER2_SHAPE_LOCATION_Y, (ushort)shape.Location.Y);
                            }
                            else if(id == (int)ManufactoryDevices.COOKIE_FILLER3_ID) {
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER3_SHAPE_LOCATION_X, (ushort)shape.Location.X);
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER3_SHAPE_LOCATION_Y, (ushort)shape.Location.Y);
                            }
                            break;
                        }
                    }                    
                } else {
                    OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.NO_COOKIES_IN_RANGE });
                }
            });
        }

        void IModbusDevice.Deactivate() {
            Program.manufactory.UpdateCoilRegister(id + 1, false);
        }
        #endregion

        #region Getters and Properties

        #endregion

        #region Overriden methods
        internal override void Activate() {
            Task.Factory.StartNew(() => {
                CookieAnimation[] shapesInRange = SearchForShapesInRange();
                if (shapesInRange.Length > 0) {
                    foreach (CookieAnimation shape in shapesInRange) {
                        if (!shape.IsFilled()) {
                            GoToLocation(shape.Location);
                            GoDown();
                            Fill(shape);
                            GoUp();
                        }
                    }
                    ReturnToTheIddlePosition();
                    OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.COOKIES_FILLED });
                    StopDemo();
                } else {
                    OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.NO_COOKIES_IN_RANGE });
                }
            });
        }        
        #endregion

        #region Private methods
        private void Fill(CookieAnimation shape) {            
            while (shape.GetCurrentFrameId() < shape.CountFrames() - 1) {
                shape.Fill();
                Thread.Sleep(30);
            }             
        }        
        #endregion

    }

    class CookieGrabber : ToolDevice, IModbusDevice {

        #region Constructor
        public CookieGrabber(int id, string name, Component guiComponent) : base(id, name, guiComponent) {

        }
        #endregion

        #region IModbus implementation
        void IModbusDevice.Start() {
            Program.manufactory.UpdateHoldingRegister(id * 10, (ushort)this.MinXPosition);
            Program.manufactory.UpdateHoldingRegister(id * 10 + 1, (ushort)this.MaxXPosition);

            Program.manufactory.UpdateCoilRegister(id * 10, true);
        }

        void IModbusDevice.Stop() {
            Program.manufactory.UpdateCoilRegister(id * 10, false);
            Program.manufactory.UpdateCoilRegister(id + 1, false);
        }

        void IModbusDevice.Activate() {
            Program.manufactory.UpdateCoilRegister(id + 1, true);
            Task.Factory.StartNew(() => {
                CookieAnimation[] shapesInRange = SearchForShapesInRange();
                if (shapesInRange.Length > 0) {
                    foreach (CookieAnimation shape in shapesInRange) {
                        if (!shape.IsFilled()) {
                            if (id == (int)ManufactoryDevices.COOKIE_FILLER1_ID) {
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER1_SHAPE_LOCATION_X, (ushort)shape.Location.X);
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER1_SHAPE_LOCATION_Y, (ushort)shape.Location.Y);
                            } else if (id == (int)ManufactoryDevices.COOKIE_FILLER2_ID) {
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER2_SHAPE_LOCATION_X, (ushort)shape.Location.X);
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER2_SHAPE_LOCATION_Y, (ushort)shape.Location.Y);
                            } else if (id == (int)ManufactoryDevices.COOKIE_FILLER3_ID) {
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER3_SHAPE_LOCATION_X, (ushort)shape.Location.X);
                                Program.manufactory.UpdateHoldingRegister(ModbusHoldingRegisters.COOKIE_FILLER3_SHAPE_LOCATION_Y, (ushort)shape.Location.Y);
                            }
                            break;
                        }
                    }
                } else {
                    OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.NO_COOKIES_IN_RANGE });
                }
            });

        }

        void IModbusDevice.Deactivate() {
            Program.manufactory.UpdateCoilRegister(id + 1, false);
        }
        #endregion

        internal override void Activate() {
            Task.Factory.StartNew(() => {
                CookieAnimation[] shapesInRange = SearchForShapesInRange();
                    if (shapesInRange.Length > 0) {
                    TrayAnimation tray = GetTrayForShape();
                    foreach (CookieAnimation shape in shapesInRange) {
                        if (shape.IsFilled()) {

                            GoToLocation(shape.Location);
                            GoDown();
                            Grabb(shape);
                            GoUp();
                            GoToFirstFreeSpaceOnTray(shape, tray);
                            GoDown();
                            Release(shape, tray);
                            GoUp();
                        }
                    }
                    ReturnToTheIddlePosition();
                    OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.COOKIES_GRABBED });
                    StopDemo();
                } else {
                    OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.NO_COOKIES_IN_RANGE });
                }
            });
        }

        private TrayAnimation GetTrayForShape() {
            return ((CookiesForm)Program.mainWindow).GetAnimationForm().GetTrayForCookieGrabber(this);
        }

        private void Grabb(CookieAnimation shape) {
            Thread.Sleep(30);
            ((CookiesForm)Program.mainWindow).GetAnimationForm().GrabbCookieFromProductionLine(ProductionLine.id, shape);
        }

        private void GoToFirstFreeSpaceOnTray(CookieAnimation withShape, TrayAnimation tray) {            
            if(tray==null && tray.isFull()) {                
                WaitForTray(1000);
                GoToFirstFreeSpaceOnTray(withShape, tray);
            }else {                
                    Point freeSpaceOnTray = tray.GetFirstFreeSpace();
                    ShiftX(freeSpaceOnTray.X, withShape);
                    ShiftY(freeSpaceOnTray.Y, withShape);               
            }
        }        

        private void WaitForTray(int time) {
            OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.WAITING_FOR_TRAY });
            Thread.Sleep(time);
        }

        private void Release(CookieAnimation shape, TrayAnimation tray) {
            Thread.Sleep(30);
            ((CookiesForm)Program.mainWindow).GetAnimationForm().ReleaseCookieFromGrabber(shape);
            tray.AddCookie(shape);
            if (tray.isFull()) {
                OnDeviceMessage(this, new DeviceMessageEventArgs() { message = DeviceMessage.TRAY_IS_FULL });
            }
        }        
    }

    public class DeviceStateChangedEventArgs : EventArgs {
        //need to change this - i am not sure, why bool value not changing when creating new instance of DeviceStateChangedEventArgs
        public bool log = true;
        public string previousState;
        public string newState;        
    }

    public class DeviceMessageEventArgs : EventArgs {
        public DeviceMessage message;        
    }

    public enum DeviceMessage {
        PRODUCTION_LINE_PERIOD_DETECTED,
        
        SHAPE_GENERATED,
        ALL_SHAPES_GENERATED,

        TOOL_DEVICE_STOPPED,
        COOKIES_FILLED,
        COOKIES_GRABBED,
        NO_COOKIES_IN_RANGE,
        WAITING_FOR_TRAY,
        TRAY_IS_FULL
    }

}
#endregion