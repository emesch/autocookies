﻿using System;

namespace AutoCookies.Model {

    internal interface IAnimated {

        Animation GetAnimation();
        void OnFrameChanged(object source, FrameChangedEventArgs e);
        void OnAnimationEnded(object source, EventArgs e);
        void OnAnimationStateChanged(object source, DeviceStateChangedEventArgs e);
        void OnAnimationSpeedChanged(object source, EventArgs args);
    }
}