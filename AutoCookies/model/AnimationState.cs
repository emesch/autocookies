﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCookies.Model {

    public enum AnimationState {
        STOPPED, STARTING, ACCELERATING, RUNNING_CONSTANT_SPEED, STOPPING
    }

    public enum ColorState {
         OFF, RED, YELLOW, GREEN
    }

    public enum CookieFillerState {
        IDDLE, SEARCHING_FOR_SHAPES, POSITIONING, FILLING, RETURNING 
    }
}
