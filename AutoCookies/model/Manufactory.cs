﻿using Modbus.Data;
using Modbus.Device;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace AutoCookies.Model {

    public sealed class Manufactory : ILogger {       

        public static Manufactory Instance {
            get {
                if (instance == null) {
                    instance = new Manufactory();
                }
                return instance;
            }
        }        

        internal void Start() {
            this.autoMode = true;
            InitializeDevices();

            this.devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR1_ID].StartDemo();
            this.devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR2_ID].StartDemo();
            this.devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR3_ID].StartDemo();
            this.devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR4_ID].StartDemo();


            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_SENSOR_ID].StartDemo();
            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_SENSOR_ID].StartDemo();
            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_SENSOR_ID].StartDemo();

            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_ID].StartDemo();
            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_ID].StartDemo();
            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_ID].StartDemo();

            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE4_ID].StartDemo();
        }        

        internal void Stop() {
            this.autoMode = false;      

            this.devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR1_ID].StopDemo();
            this.devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR2_ID].StopDemo();
            this.devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR3_ID].StopDemo();
            this.devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR4_ID].StopDemo();

            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_ID].StopDemo();
            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_ID].StopDemo();
            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_ID].StopDemo();

            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE4_ID].StopDemo();

            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_SENSOR_ID].StopDemo();
            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_SENSOR_ID].StopDemo();
            this.devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_SENSOR_ID].StopDemo();
        }

        internal void StartForModbus() {            
            InitializeDevices();
            foreach(IModbusDevice modDev in devicesMap.Values) {
                modDev.Start();
            }
        }

        internal void StopForModbus() {
            foreach(IModbusDevice modDev in devicesMap.Values) {
                modDev.Stop();
            }            
        }

        public void OpenSerialPort(string portName, string baud) {
            serialPort.PortName = portName;
            int baudRate = 9600;
            if (!baud.Equals("")) {
                int.TryParse(baud, out baudRate);
            }
            serialPort.BaudRate = baudRate;
            serialPort.Open();
        }

        public void CloseSerialPort() {
            serialPort.Close();
        }

        public void AddDevice(Device device) {
            devicesMap.Add(device.id, device);
            device.deviceStateChanged += OnDeviceStateChanged;
            device.deviceMessage += OnDeviceMessage;
            this.manufactoryMessage += device.OnManufactoryMessage;                       
        }

        private void InitializeDevices() {
            ConnectSensors();
            AdjustSensors();
            ConnectShapeGenerators();
            AdjustShapeGenerators();
            ConnectCookieTools(); 
        }        

        private void ConnectSensors() {
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_ID]).Sensor = (ProductionLineSensor)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_SENSOR_ID];
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_ID]).Sensor = (ProductionLineSensor)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_SENSOR_ID];
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_ID]).Sensor = (ProductionLineSensor)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_SENSOR_ID];
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE4_ID]).Sensor = (ProductionLineSensor)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE4_SENSOR_ID];
        }        

        private void AdjustSensors() {
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE4_ID]).Sensor.Interspace = 150;
        }

        private void ConnectShapeGenerators() {
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_ID]).ShapeGenerator = (ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR1_ID];
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_ID]).ShapeGenerator = (ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR2_ID];
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_ID]).ShapeGenerator = (ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR3_ID];
            ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE4_ID]).ShapeGenerator = (ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR4_ID];
        }

        private void ConnectCookieTools() {
            ((CookieFiller)devicesMap[(int)ManufactoryDevices.COOKIE_FILLER1_ID]).ProductionLine = (ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_ID];
            ((CookieFiller)devicesMap[(int)ManufactoryDevices.COOKIE_FILLER2_ID]).ProductionLine = (ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_ID];
            ((CookieFiller)devicesMap[(int)ManufactoryDevices.COOKIE_FILLER3_ID]).ProductionLine = (ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_ID];

            ((CookieGrabber)devicesMap[(int)ManufactoryDevices.COOKIE_GRABBER1_ID]).ProductionLine = (ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_ID];
            ((CookieGrabber)devicesMap[(int)ManufactoryDevices.COOKIE_GRABBER2_ID]).ProductionLine = (ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE2_ID];
            ((CookieGrabber)devicesMap[(int)ManufactoryDevices.COOKIE_GRABBER3_ID]).ProductionLine = (ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE3_ID];
        }

        private void AdjustShapeGenerators() {
            //Horizontal ShapeGenerators
            int x1 = ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_ID]).GetAnimation().Location.X + 50;
            int y1 = ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE1_ID]).GetAnimation().Location.Y + 20;

            ((ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR1_ID]).AdjustGeneratedShapeLocation(0, new Point(x1, y1));
            ((ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR2_ID]).AdjustGeneratedShapeLocation(0, new Point(x1, y1+150)); 
            ((ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR3_ID]).AdjustGeneratedShapeLocation(0, new Point(x1, y1+300));

            //Vertical ShapeGenerator
            int x2 = ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE4_ID]).GetAnimation().Location.X + 10;
            int y2 = ((ProductionLine)devicesMap[(int)ManufactoryDevices.PRODUCTION_LINE4_ID]).GetAnimation().Location.Y + 70;

            ((ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR4_ID]).AdjustGeneratedShapeLocation(0, new Point(x2, y2));
            ((ShapeGenerator)devicesMap[(int)ManufactoryDevices.SHAPE_GENERATOR4_ID]).TypeOfShape = Animations.TRAY;
        }        

        public void OnManufactoryMessage(object source, ManufactoryMessageEventArgs e) {
            if (manufactoryMessage != null) {
                manufactoryMessage(this, e);
            }
        }

        private void OnDeviceStateChanged(object source, DeviceStateChangedEventArgs args) {
            Device device = (Device)source;
            if(args.log) {
                //WriteLog("Device (" + device.id + ", " + device.name + ") state changed from " + args.previousState + " to " + args.newState);
            }
            if (autoMode) {
                if (device is ProductionLine && args.newState.Equals("STOPPED")) {
                    OnManufactoryMessage(this, new ManufactoryMessageEventArgs { message = ManufactoryMessage.PRODUCTION_LINE_STOPPED, intValue = device.id });
                }
                if (device is ProductionLineSensor && args.newState.Equals("GREEN")) {
                    ActivateShapeGenerator(((ProductionLineSensor)device).ProductionLine.ShapeGenerator);
                    ActivateCookieFiller(((ProductionLineSensor)device).ProductionLine.CookieFiller);
                    ActivateCookieGrabber(((ProductionLineSensor)device).ProductionLine.CookieGrabber);
                }
            }
                       
        }        

        private void OnDeviceMessage(object source, DeviceMessageEventArgs args) {
            Device device = (Device)source;
            processMessage(device, args.message);
        }        

        private void WriteLog(string log) {
            ((CookiesForm)Program.mainWindow).WriteLog(this, log);
        }

        internal Device GetDevice(int id) {
            Device device;
            if(devicesMap.TryGetValue(id, out device)){
                return devicesMap[id];
            }
            return device;
        }

        private IModbusDevice GetModbusDevice(int id) {
            return (IModbusDevice)GetDevice(id);
        }

        public void StartModbusSlaveListener() {
            Task.Factory.StartNew(() => {
                modbusSlave.DataStore.DataStoreWrittenTo += new EventHandler<DataStoreEventArgs>(Modbus_DataStoreWriteTo);                
                modbusSlave.Listen();
            });
        }       

        private Manufactory() {
            this.devicesMap = new Dictionary<int, Device>();
            this.isTrayFull = new bool[3];

            InitializeSerialPort();
            InitializeModbusSlave();           
        }       

        private void InitializeSerialPort() {
            serialPort = new System.IO.Ports.SerialPort();
            serialPort.BaudRate = 9600;
            serialPort.DataBits = 8;
            serialPort.Parity = System.IO.Ports.Parity.Even;
            serialPort.StopBits = System.IO.Ports.StopBits.One;
        }        

        private void InitializeModbusSlave() {
            modbusSlave = ModbusSerialSlave.CreateRtu(1, serialPort);
            modbusSlave.DataStore = DataStoreFactory.CreateDefaultDataStore();
        }

        public string GetLoggerName() {
            return "Manufactory";
        }

        #region Private modbusFunctions
        private void Modbus_DataStoreWriteTo(object sender, DataStoreEventArgs e) {
            switch (e.ModbusDataType) {
                case Modbus.Data.ModbusDataType.Coil:
                    ushort registerAddress1 = e.StartAddress;
                    var receivedData1 = e.Data.A;
                    WriteDataFromMasterToCoilRegister(registerAddress1, receivedData1);
                    break;
                case Modbus.Data.ModbusDataType.HoldingRegister:
                    ushort registerAddress2 = e.StartAddress;
                    var receivedData2 = e.Data.B;
                    WriteDataFromMasterToHoldingRegister(registerAddress2, receivedData2);
                    break;
            }
        }

        private void WriteDataFromMasterToCoilRegister(ushort registerAddress, ReadOnlyCollection<bool> receivedData) {
            WriteLog("Coil register ID: " + registerAddress + " updated with value: " + receivedData.Last());
            IModbusDevice modbusDevice = GetModbusDeviceByCoilRegisterAddress(registerAddress);
            if (receivedData.Last() == true) {
                if (registerAddress % 10 == 0) {
                    modbusDevice.Start();
                } else if(GetCoilRegisterValue(registerAddress - 1)) {
                        if (registerAddress % 10 == 1 ) {
                            modbusDevice.Activate();
                        } else if (registerAddress % 10 == 2 ) {
                            if(modbusDevice is ToolDevice) {
                                ushort toNextXLocation = Program.manufactory.GetHoldingRegisterValue(((Device)modbusDevice).id * 10 + 2); 
                                if (toNextXLocation != 0) {
                                    ((ToolDevice)modbusDevice).ShiftX(toNextXLocation);
                                }                                
                            }
                        }
                } else {
                        UpdateCoilRegister(registerAddress, false);
                    }
                                 
            } else {
                if(registerAddress % 10 == 0) {
                    modbusDevice.Stop();
                }else if(registerAddress % 10 == 1) {
                    modbusDevice.Deactivate();
                }
                
            } 
        }        

        private void WriteDataFromMasterToHoldingRegister(ushort registerAddress, ReadOnlyCollection<ushort> receivedData) {
            WriteLog("Holding register ID: " + registerAddress + " updated with value: " + receivedData.Last());

            if(registerAddress == GetModbusRegister(ModbusHoldingRegisters.PRODUCTION_LINE1_MAX_SPEED)) {
                ((ProductionLine)GetDevice((int)ManufactoryDevices.PRODUCTION_LINE1_ID)).MaxSpeed = modbusSlave.DataStore.HoldingRegisters[registerAddress+1];
            }else if(registerAddress == GetModbusRegister(ModbusHoldingRegisters.PRODUCTION_LINE2_MAX_SPEED)) {
                ((ProductionLine)GetDevice((int)ManufactoryDevices.PRODUCTION_LINE2_ID)).MaxSpeed = modbusSlave.DataStore.HoldingRegisters[registerAddress+1];
            }else if(registerAddress == GetModbusRegister(ModbusHoldingRegisters.PRODUCTION_LINE3_MAX_SPEED)) {
                ((ProductionLine)GetDevice((int)ManufactoryDevices.PRODUCTION_LINE3_ID)).MaxSpeed = modbusSlave.DataStore.HoldingRegisters[registerAddress + 1];
            }else if(registerAddress == GetModbusRegister(ModbusHoldingRegisters.COOKIE_FILLER1_NEXT_X_POSITION)) {

            }
        }

        internal void UpdateHoldingRegister(ModbusHoldingRegisters holdingRegister, ushort v) {
            modbusSlave.DataStore.HoldingRegisters[(int)holdingRegister+1] = v;
        }

        internal void UpdateHoldingRegister(int holdingRegister, ushort v) {
            modbusSlave.DataStore.HoldingRegisters[(int)holdingRegister + 1] = v;
        }

        internal void UpdateCoilRegister(ModbusCoilRegisters coilRegister, bool v) {
            modbusSlave.DataStore.CoilDiscretes[(int)coilRegister + 1] = v;
        }

        internal void UpdateCoilRegister(int coilRegister, bool v) {
            modbusSlave.DataStore.CoilDiscretes[(int)coilRegister + 1] = v;
        }

        internal bool GetCoilRegisterValue(int id) {
            return modbusSlave.DataStore.CoilDiscretes[id+1];
        }

        private ushort GetHoldingRegisterValue(int id) {
            return modbusSlave.DataStore.HoldingRegisters[id + 1];
        }

        private IModbusDevice GetModbusDeviceByCoilRegisterAddress(ushort registerAddress) {
            IModbusDevice returnedDevice = null;
            if(registerAddress >= 0 && registerAddress <= 9) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.PRODUCTION_LINE1_ID);
            }
            if(registerAddress >= 10 && registerAddress <= 19) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.PRODUCTION_LINE2_ID);
            }
            if(registerAddress >= 20 && registerAddress <= 29) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.PRODUCTION_LINE3_ID);
            }
            if(registerAddress >= 30 && registerAddress <= 39) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.SHAPE_GENERATOR1_ID);
            }
            if(registerAddress >= 40 && registerAddress <= 49) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.SHAPE_GENERATOR2_ID);
            }
            if (registerAddress >= 50 && registerAddress <= 59) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.SHAPE_GENERATOR3_ID);
            }
            if (registerAddress >= 60 && registerAddress <= 69) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.PRODUCTION_LINE1_SENSOR_ID);
            }
            if (registerAddress >= 70 && registerAddress <= 79) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.PRODUCTION_LINE2_SENSOR_ID);
            }
            if (registerAddress >= 80 && registerAddress <= 89) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.PRODUCTION_LINE3_SENSOR_ID);
            }
            if (registerAddress >= 90 && registerAddress <= 99) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.COOKIE_FILLER1_ID);
            }
            if (registerAddress >= 100 && registerAddress <= 109) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.COOKIE_FILLER2_ID);
            }
            if (registerAddress >= 110 && registerAddress <= 119) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.COOKIE_FILLER3_ID);
            }
            if (registerAddress >= 120 && registerAddress <= 129) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.COOKIE_GRABBER1_ID);
            }
            if (registerAddress >= 130 && registerAddress <= 139) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.COOKIE_GRABBER2_ID);
            }
            if (registerAddress >= 140 && registerAddress <= 149) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.COOKIE_GRABBER3_ID);
            }
            if (registerAddress >= 150 && registerAddress <= 159) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.PRODUCTION_LINE4_ID);
            }
            if (registerAddress >= 160 && registerAddress <= 169) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.PRODUCTION_LINE4_SENSOR_ID);
            }
            if (registerAddress >= 170 && registerAddress <= 179) {
                return returnedDevice = GetModbusDevice((int)ManufactoryDevices.SHAPE_GENERATOR4_ID);
            }

            return returnedDevice;
        }        
        #endregion

        public delegate void ManufactoryMessageEventHandler(object source, ManufactoryMessageEventArgs args);
        public event ManufactoryMessageEventHandler manufactoryMessage;

        private void processMessage(Device sender, DeviceMessage message) {
            if (this.autoMode) {
                switch (message) {
                    case DeviceMessage.PRODUCTION_LINE_PERIOD_DETECTED:
                        if (sender is ProductionLineSensor) {
                            StopProductionLineBySensor((ProductionLineSensor)sender);
                        }
                        break;
                    case DeviceMessage.ALL_SHAPES_GENERATED:
                        ProductionLine pl = ((ShapeGenerator)sender).ProductionLine;
                        pl.AllShapesGenerated();
                        int objectsOnPl = ((CookiesForm)Program.mainWindow).GetAnimationForm().CountObjectsOnProductionLine(pl.id);
                        if (pl.id == (int)ManufactoryDevices.PRODUCTION_LINE4_ID && objectsOnPl >= 3) {
                            pl.Waiting = true;
                        }
                        WriteLog("Objects on production line ID: " + pl.id + ": " + objectsOnPl);
                        if (pl.IsReadyToMove()) {
                            pl.StartDemo();
                        }
                        break;
                    case DeviceMessage.COOKIES_FILLED:
                        ProductionLine pline = ((CookieFiller)sender).ProductionLine;
                        pline.CookiesInRangeFilled();
                        if (pline.IsReadyToMove()) {
                            pline.StartDemo();
                        }
                        break;
                    case DeviceMessage.COOKIES_GRABBED:
                        ProductionLine plinex = ((CookieGrabber)sender).ProductionLine;
                        plinex.CookiesInRangeGrabbed();
                        if (plinex.IsReadyToMove()) {
                            plinex.StartDemo();
                        }
                        break;
                    case DeviceMessage.NO_COOKIES_IN_RANGE:
                        ProductionLine prodLine = ((ToolDevice)sender).ProductionLine;
                        if (sender is CookieFiller) {
                            prodLine.CookiesInRangeFilled();
                        } else if (sender is CookieGrabber) {
                            prodLine.CookiesInRangeGrabbed();
                        }
                        if (prodLine.IsReadyToMove()) {
                            prodLine.StartDemo();
                        }
                        break;
                    case DeviceMessage.TRAY_IS_FULL:
                        ToolDevice tool = ((ToolDevice)sender);
                        ProductionLine prodLine4 = (ProductionLine)GetDevice((int)ManufactoryDevices.PRODUCTION_LINE4_ID);
                        switch (tool.id) {
                            case (int)ManufactoryDevices.COOKIE_GRABBER1_ID:
                                isTrayFull[0] = true;
                                if (allTraysFull()) {
                                    prodLine4.StartDemo();
                                    ResetTrayFlags();
                                }
                                break;
                            case (int)ManufactoryDevices.COOKIE_GRABBER2_ID:
                                isTrayFull[1] = true;
                                if (allTraysFull()) {
                                    prodLine4.StartDemo();
                                    ResetTrayFlags();
                                }
                                break;
                            case (int)ManufactoryDevices.COOKIE_GRABBER3_ID:
                                isTrayFull[2] = true;
                                if (allTraysFull()) {
                                    prodLine4.StartDemo();
                                    ResetTrayFlags();
                                }
                                break;
                        }

                        break;
                }
            }
            
        }

        private void ResetTrayFlags() {
            isTrayFull[0] = false;
            isTrayFull[1] = false;
            isTrayFull[2] = false;
        }

        private bool allTraysFull() {
            return isTrayFull[0] && isTrayFull[1] && isTrayFull[2];
        }

        private void StopProductionLineBySensor(ProductionLineSensor sensor) {
            sensor.ProductionLine.StopDemo();
        }

        private void ActivateShapeGenerator(ShapeGenerator generator) {
            if(generator != null) {
                generator.Activate();
            }            
        }        

        private void ActivateCookieFiller(CookieFiller filler) {
            if(filler != null) {
                filler.Activate();
            }            
        }

        private void ActivateCookieGrabber(CookieGrabber grabber) {
            if (grabber != null) {
                grabber.Activate();
            }
        }

        private ushort GetModbusRegister(ModbusHoldingRegisters reg) {
            return (ushort)reg;
        }

        private static Manufactory instance;
        private bool autoMode = false;
        private ModbusSlave modbusSlave;
        private SerialPort serialPort;
        private Dictionary<int, Device> devicesMap;

        private bool[] isTrayFull;        
    }

    public enum ManufactoryDevices : int {

        PRODUCTION_LINE1_ID = 0,
        PRODUCTION_LINE2_ID = 1,
        PRODUCTION_LINE3_ID = 2,
        SHAPE_GENERATOR1_ID = 3,
        SHAPE_GENERATOR2_ID = 4,
        SHAPE_GENERATOR3_ID = 5,
        PRODUCTION_LINE1_SENSOR_ID = 6,
        PRODUCTION_LINE2_SENSOR_ID = 7,
        PRODUCTION_LINE3_SENSOR_ID = 8,
        COOKIE_FILLER1_ID = 9,
        COOKIE_FILLER2_ID = 10,
        COOKIE_FILLER3_ID = 11,
        COOKIE_GRABBER1_ID = 12,
        COOKIE_GRABBER2_ID = 13,
        COOKIE_GRABBER3_ID = 14,
        PRODUCTION_LINE4_ID = 15,
        PRODUCTION_LINE4_SENSOR_ID = 16,
        SHAPE_GENERATOR4_ID = 17,
    }    

    public class ManufactoryMessageEventArgs : EventArgs {
        public ManufactoryMessage message;
        public int intValue;
    }

    public enum ManufactoryMessage {
        PRODUCTION_LINE_STOPPED
    }

}
