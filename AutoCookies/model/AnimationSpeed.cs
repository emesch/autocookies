﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCookies.Model {

    class AnimationSpeed {

        public static readonly double DEFAULT_ACCELERATION_VALUE = 1;

        #region Constructor
        public AnimationSpeed(Animation animation, double startSpeed, double maxSpeed, bool isAccelerating) {
            this.animation = animation;
            this.currentSpeed = 0;
            this.startSpeed = startSpeed;
            this.maxSpeed = maxSpeed;

            this.accelerated = isAccelerating;
            this.accelerationValue = DEFAULT_ACCELERATION_VALUE;
        }
        #endregion

        #region Public getters
        public double GetCurrentSpeed() {
            return this.currentSpeed;
        }

        internal double GetMaxSpeed() {
            return this.maxSpeed;
        }

        /// <summary>        
        /// Throws InvalidOperationException when current animation speed = 0
        /// </summary>
        public double GetTimerIntervalForCurrentSpeed() {
            if (GetCurrentSpeed() != 0) {
                return 1000 / GetCurrentSpeed();
            } else throw new InvalidOperationException("Impossible to get timer interval, because current animation speed = 0.");
        }

        /// <summary>        
        /// Throws InvalidOperationException when current animation speed = 0
        /// </summary>
        public int GetSuspendTimeForCurrentSpeed() {
            if (GetCurrentSpeed() != 0) {
                return (int)(1000 / GetCurrentSpeed());
            } else throw new InvalidOperationException("Impossible to get suspend, because current animation speed = 0.");
        }

        public bool IsAccelerated() {
            return this.accelerated;
        }

        public double GetAccelerationValue() {
            return this.accelerationValue;
        }

        public AnimationState GetState() {
            return this.animation.GetState();
        }
        #endregion

        #region Public interface
        public void SetCurrentSpeedByAnimationTimerInterval(double timerInterval) {
            double newSpeed = 1000 / timerInterval;
            SetCurrentSpeed(newSpeed);
        }        

        public void SetAcceleration(bool accelerated, double accelerationValue, double maxSpeed) {
            this.accelerated = accelerated;
            this.accelerationValue = accelerationValue;
            this.maxSpeed = (double)maxSpeed;
        }

        public void Update() {
            switch (GetState()) {
                case AnimationState.STOPPED:
                    SetCurrentSpeed(0);
                    break;
                case AnimationState.STARTING:
                    SetCurrentSpeed(this.startSpeed);
                    break;
                case AnimationState.ACCELERATING:
                    Increase();
                    break;
                case AnimationState.STOPPING:
                    Reduce();
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Private methods
        private void Increase() {
            if (this.currentSpeed < this.maxSpeed) {
                SetCurrentSpeed(this.currentSpeed * this.accelerationValue);
            }
        }

        private void Reduce() {
            if (this.currentSpeed > this.startSpeed) {
                SetCurrentSpeed(this.currentSpeed / this.accelerationValue);
            } else {
                SetCurrentSpeed(0);
            }
        }

        private void SetCurrentSpeed(double speed) {
            bool changed = false;
            if (speed != this.currentSpeed) {
                this.currentSpeed = speed;
                if (this.currentSpeed > this.maxSpeed) {
                    this.currentSpeed = this.maxSpeed;
                }
                changed = true;
            }
            if (changed) {
                //this.animation.UpdateTickSpeed();
                OnSpeedChanged();
            }

        }
        #endregion

        #region OnEvent methods
        private void OnSpeedChanged() {
            this.animation.OnCurrentSpeedChanged();
        }
        #endregion

        private Animation animation;

        private double currentSpeed;
        private double startSpeed;
        private double maxSpeed;

        private bool accelerated;
        private double accelerationValue;                
    }
}
