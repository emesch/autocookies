﻿using AutoCookies.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoCookies {

    static class Program {

        public static Manufactory manufactory = Manufactory.Instance;
        public static Form mainWindow;

        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            mainWindow = new CookiesForm();
            Application.Run(mainWindow);
        }
    }
}
