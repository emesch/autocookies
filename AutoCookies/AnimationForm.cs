﻿using AutoCookies.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoCookies {

    public partial class AnimationForm : Form {

        public static readonly Type COOKIE_TYPE = typeof(CookieAnimation);
        public static readonly Type TRAY_TYPE = typeof(TrayAnimation);

        public AnimationForm() {
            InitializeComponent();

            this.pLineObjectGroups = new Dictionary<int, Dictionary<int, Animation>>();
            this.objectGroup1 = new Dictionary<int, Animation>();
            this.objectGroup2 = new Dictionary<int, Animation>();
            this.objectGroup3 = new Dictionary<int, Animation>();
            this.objectGroup4 = new Dictionary<int, Animation>();
            this.pLineObjectGroups.Add((int)ManufactoryDevices.PRODUCTION_LINE1_ID, objectGroup1);
            this.pLineObjectGroups.Add((int)ManufactoryDevices.PRODUCTION_LINE2_ID, objectGroup2);
            this.pLineObjectGroups.Add((int)ManufactoryDevices.PRODUCTION_LINE3_ID, objectGroup3);
            this.pLineObjectGroups.Add((int)ManufactoryDevices.PRODUCTION_LINE4_ID, objectGroup4);

            this.trayQueue = new Queue<TrayAnimation>();
        }

        public Manufactory Manufactory() {
            return Program.manufactory;
        }

        private void loadAnimationForm(object sender, EventArgs e) {
            InitializeAnimationComponents();
            InitializeManufactoryDevices();
        }

        private void InitializeAnimationComponents() {
            this.shapeGenerator1Animation = AnimationFactory.createAnimation("shapeGenerator1Animation", Animations.SHAPE_GENERATOR, 50, 80);
            this.shapeGenerator2Animation = AnimationFactory.createAnimation("shapeGenerator2Animation", Animations.SHAPE_GENERATOR, 50, 230);
            this.shapeGenerator3Animation = AnimationFactory.createAnimation("shapeGenerator3Animation", Animations.SHAPE_GENERATOR, 50, 380);            
            this.Controls.Add(this.shapeGenerator1Animation);
            this.Controls.Add(this.shapeGenerator2Animation);
            this.Controls.Add(this.shapeGenerator3Animation);

            this.cookieFiller1Animation = AnimationFactory.createAnimation("cookieFiller1Animation", Animations.XYZ_TOOL, 270, 65);
            this.cookieFiller2Animation = AnimationFactory.createAnimation("cookieFiller2Animation", Animations.XYZ_TOOL, 270, 215);
            this.cookieFiller3Animation = AnimationFactory.createAnimation("cookieFiller3Animation", Animations.XYZ_TOOL, 270, 365);
            this.Controls.Add(this.cookieFiller1Animation);
            this.Controls.Add(this.cookieFiller2Animation);
            this.Controls.Add(this.cookieFiller3Animation);

            this.cookieGrabber1Animation = AnimationFactory.createAnimation("cookieGrabber1Animation", Animations.XYZ_TOOL, 370, 65);
            this.cookieGrabber2Animation = AnimationFactory.createAnimation("cookieGrabber2Animation", Animations.XYZ_TOOL, 370, 215);
            this.cookieGrabber3Animation = AnimationFactory.createAnimation("cookieGrabber3Animation", Animations.XYZ_TOOL, 370, 365);
            this.Controls.Add(this.cookieGrabber1Animation);
            this.Controls.Add(this.cookieGrabber2Animation);
            this.Controls.Add(this.cookieGrabber3Animation);

            this.shapeGenerator4Animation = AnimationFactory.createAnimation("shapeGenerator4Animation", Animations.SHAPE_GENERATOR, 525, -10);            
            this.Controls.Add(this.shapeGenerator4Animation);

            this.productionLine4Animation = AnimationFactory.createAnimation("productionLine4Animation", Animations.VERTICAL_PRODUCTION_LINE, 490, 30);
            this.Controls.Add(this.productionLine4Animation);

            this.pLine4SensorLight = AnimationFactory.createAnimation("pLine4SensorLight", Animations.PRODUCTION_LINE_SENSOR_LIGHT, 440, 50);
            this.Controls.Add(this.pLine4SensorLight);


            this.productionLine1Animation = AnimationFactory.createAnimation("productionLine1Animation", Animations.PRODUCTION_LINE, 100, 100);
            this.productionLine2Animation = AnimationFactory.createAnimation("productionLine2Animation", Animations.PRODUCTION_LINE, 100, 250);
            this.productionLine3Animation = AnimationFactory.createAnimation("productionLine3Animation", Animations.PRODUCTION_LINE, 100, 400);
            this.Controls.Add(this.productionLine1Animation);
            this.Controls.Add(this.productionLine2Animation);
            this.Controls.Add(this.productionLine3Animation);

            this.pLine1SensorLight = AnimationFactory.createAnimation("pLine1SensorLight", Animations.PRODUCTION_LINE_SENSOR_LIGHT, 172, 70);
            this.pLine2SensorLight = AnimationFactory.createAnimation("pLine2SensorLight", Animations.PRODUCTION_LINE_SENSOR_LIGHT, 172, 220);
            this.pLine3SensorLight = AnimationFactory.createAnimation("pLine3SensorLight", Animations.PRODUCTION_LINE_SENSOR_LIGHT, 172, 350);
            this.Controls.Add(this.pLine1SensorLight);
            this.Controls.Add(this.pLine2SensorLight);
            this.Controls.Add(this.pLine3SensorLight);



        }

        private void InitializeManufactoryDevices() {
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.PRODUCTION_LINE1_ID, "productionLine1", Devices.PRODUCTION_LINE, this.productionLine1Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.PRODUCTION_LINE2_ID, "productionLine2", Devices.PRODUCTION_LINE, this.productionLine2Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.PRODUCTION_LINE3_ID, "productionLine3", Devices.PRODUCTION_LINE, this.productionLine3Animation));

            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.SHAPE_GENERATOR1_ID, "shapeGenerator1", Devices.SHAPE_GENERATOR, this.shapeGenerator1Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.SHAPE_GENERATOR2_ID, "shapeGenerator2", Devices.SHAPE_GENERATOR, this.shapeGenerator2Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.SHAPE_GENERATOR3_ID, "shapeGenerator3", Devices.SHAPE_GENERATOR, this.shapeGenerator3Animation));

            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.PRODUCTION_LINE1_SENSOR_ID, "pLineSensor1", Devices.PRODUCTION_LINE_SENSOR, this.pLine1SensorLight));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.PRODUCTION_LINE2_SENSOR_ID, "pLineSensor2", Devices.PRODUCTION_LINE_SENSOR, this.pLine2SensorLight));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.PRODUCTION_LINE3_SENSOR_ID, "pLineSensor3", Devices.PRODUCTION_LINE_SENSOR, this.pLine3SensorLight));

            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.COOKIE_FILLER1_ID, "cFiller1", Devices.COOKIE_FILLER, this.cookieFiller1Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.COOKIE_FILLER2_ID, "cFiller2", Devices.COOKIE_FILLER, this.cookieFiller2Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.COOKIE_FILLER3_ID, "cFiller3", Devices.COOKIE_FILLER, this.cookieFiller3Animation));

            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.PRODUCTION_LINE4_ID, "productionLine4", Devices.PRODUCTION_LINE, this.productionLine4Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.PRODUCTION_LINE4_SENSOR_ID, "pLineSensor4", Devices.PRODUCTION_LINE_SENSOR, this.pLine4SensorLight));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.SHAPE_GENERATOR4_ID, "shapeGenerator4", Devices.SHAPE_GENERATOR, this.shapeGenerator4Animation));

            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.COOKIE_GRABBER1_ID, "cGrabber1", Devices.COOKIE_GRABBER, this.cookieGrabber1Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.COOKIE_GRABBER2_ID, "cGrabber2", Devices.COOKIE_GRABBER, this.cookieGrabber2Animation));
            Manufactory().AddDevice(DeviceFactory.createDevice((int)ManufactoryDevices.COOKIE_GRABBER3_ID, "cGrabber3", Devices.COOKIE_GRABBER, this.cookieGrabber3Animation));

        }        

        internal void GenerateShape(Animations typeOfShape, int pLineId, int x, int y) {
            Action action = () => {
                Animation generatedShape = GenerateAnimationByType(typeOfShape, x, y);
                if(generatedShape is CookieAnimation) {
                    this.pLineObjectGroups[pLineId].Add(((CookieAnimation)generatedShape).id, generatedShape);
                }else if(generatedShape is TrayAnimation) {
                    this.pLineObjectGroups[pLineId].Add(((TrayAnimation)generatedShape).id, generatedShape);
                    this.trayQueue.Enqueue((TrayAnimation)generatedShape);
                }               
                this.Controls.Add(generatedShape);

                generatedShape.BringToFront();
                this.productionLine4Animation.BringToFront();
                this.shapeGenerator4Animation.BringToFront();
                var objectsOnLine4 = this.pLineObjectGroups[(int)ManufactoryDevices.PRODUCTION_LINE4_ID];
                foreach(Animation animation in objectsOnLine4.Values) {
                    animation.BringToFront();
                }
            };
            this.Invoke(action);
        }

        private Animation GenerateAnimationByType(Animations typeOfShape, int x, int y) {
            Animation generatedAnimation = null; 
            switch(typeOfShape) {
                case Animations.COOKIE:
                    generatedAnimation = AnimationFactory.createAnimation("cookie", Animations.COOKIE, x, y);
                    break;
                case Animations.TRAY:
                    generatedAnimation = AnimationFactory.createAnimation("tray", Animations.TRAY, x, y);
                    break;
            }
            return generatedAnimation;
        }

        internal int CountObjectsOnProductionLine(int productionLineId) {
            return this.pLineObjectGroups[productionLineId].Count;
        }

        internal void MoveAllObjectsOnPLine(int pLineId) {
            Action action = () => {
                var objectsOnLine = this.pLineObjectGroups[pLineId];
                foreach (Animation objectOnLine in objectsOnLine.Values) {
                    Point p = objectOnLine.Location;
                    if(pLineId != (int)ManufactoryDevices.PRODUCTION_LINE4_ID) {
                        objectOnLine.Location = new Point(p.X + 2, p.Y);
                    }else {
                        objectOnLine.Location = new Point(p.X, p.Y + 2);
                    }                                       
                }
                Animation[] cookiesToRemove = null;
                Animation[] traysAndCookiesToRemove = null;
                              
                cookiesToRemove = objectsOnLine.Values.Where(c => c.Location.X >= 700).ToArray();
                foreach (CookieAnimation cookie in cookiesToRemove) {
                    objectsOnLine.Remove(cookie.id);
                    cookie.Dispose();
                }
                traysAndCookiesToRemove = objectsOnLine.Values.Where(c => c.Location.Y >= 500).ToArray();
                foreach (Animation trayOrCookie in traysAndCookiesToRemove) {
                    if(trayOrCookie is CookieAnimation) {
                        objectsOnLine.Remove(((CookieAnimation)trayOrCookie).id);
                    } else if (trayOrCookie is TrayAnimation) {
                        objectsOnLine.Remove(((TrayAnimation)trayOrCookie).id);
                    }
                                        
                    trayOrCookie.Dispose();
                }                             
            };
            this.Invoke(action);
        }

        internal void GrabbCookieFromProductionLine(int pLineId, CookieAnimation cookie) {
            var objectsOnLine = this.pLineObjectGroups[pLineId];
            objectsOnLine.Remove(cookie.id);
        }

        internal void ReleaseCookieFromGrabber(CookieAnimation shape) {
            this.pLineObjectGroups[(int)ManufactoryDevices.PRODUCTION_LINE4_ID].Add(shape.id, shape);
        }

        internal void MoveAnimationElement(Animation animation, Point point) {
            Action action = () => {
                animation.Location = point;
                animation.BringToFront();
            };
            this.Invoke(action);
        }

        internal void GoDown(Animation animation) {
            Action action = () => {
                animation.NextFrame();
                animation.SetBounds(animation.Location.X, animation.Location.Y, animation.CurrentFrame().Width, animation.CurrentFrame().Height);
            };
            this.Invoke(action);
            
        }

        internal void GoUp(Animation animation) {
            Action action = () => {
                animation.PreviousFrame();
                animation.SetBounds(animation.Location.X, animation.Location.Y, animation.CurrentFrame().Width, animation.CurrentFrame().Height);
            };
            this.Invoke(action);
            
        }

        internal CookieAnimation[] GetCookiesInRange(int pLineId, int minXPosition, int maxXPosition) {            
            var objectsOnLine = this.pLineObjectGroups[pLineId];
            var objectsInRange = objectsOnLine.Values.Where(c => c.Location.X >= minXPosition && c.Location.X <= maxXPosition).ToArray();

            CookieAnimation[] cookiesInRange = new CookieAnimation[objectsInRange.Length];
            for(int i=0; i<objectsInRange.Length; i++) {
                cookiesInRange[i] = (CookieAnimation)objectsInRange[i];
            }
            return cookiesInRange;
        }

        internal TrayAnimation GetTrayForCookieGrabber(CookieGrabber cookieGrabber) {
            TrayAnimation returnedTray = null;
            
            if (trayQueue.Count == 3) {
                if(this.firstTray == null && this.secondTray == null && this.thirdTray == null) {
                    this.thirdTray = this.trayQueue.Dequeue();
                    this.secondTray = this.trayQueue.Dequeue();
                    this.firstTray = this.trayQueue.Dequeue();
                }               
            }
            switch (cookieGrabber.id) {
                case (int)ManufactoryDevices.COOKIE_GRABBER1_ID:
                    returnedTray = (TrayAnimation)this.firstTray;
                    break;
                case (int)ManufactoryDevices.COOKIE_GRABBER2_ID:
                    returnedTray = (TrayAnimation)this.secondTray;
                    break;
                case (int)ManufactoryDevices.COOKIE_GRABBER3_ID:
                    returnedTray = (TrayAnimation)this.thirdTray;
                    break;
            }
            return returnedTray;
        }        

        private Animation productionLine1Animation;
        private Animation productionLine2Animation;
        private Animation productionLine3Animation;

        private Animation shapeGenerator1Animation;
        private Animation shapeGenerator2Animation;
        private Animation shapeGenerator3Animation;
        private Animation shapeGenerator4Animation;

        private Animation pLine1SensorLight;
        private Animation pLine2SensorLight;
        private Animation pLine3SensorLight;

        private Animation cookieFiller1Animation;
        private Animation cookieFiller2Animation;
        private Animation cookieFiller3Animation;

        private Animation cookieGrabber1Animation;
        private Animation cookieGrabber2Animation;
        private Animation cookieGrabber3Animation;

        private Animation productionLine4Animation;
        private Animation pLine4SensorLight;

        private Animation firstTray;
        private Animation secondTray;
        private Animation thirdTray;

        private Dictionary<int, Dictionary<int, Animation>> pLineObjectGroups;
        private Dictionary<int, Animation> objectGroup1;
        private Dictionary<int, Animation> objectGroup2;
        private Dictionary<int, Animation> objectGroup3;
        private Dictionary<int, Animation> objectGroup4;
        private Queue<TrayAnimation> trayQueue;        
    }   
    
}
