﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modbus.Data;
using Modbus.Device;
using AutoCookies.Model;
using System.Drawing.Imaging;
using System.Threading;
using System.IO;

namespace AutoCookies {

    public partial class CookiesForm : Form, ILogger {        

        public CookiesForm() {            
            InitializeComponent();                       
        }        

        public void WriteLog(ILogger logger, string txt) {            
            Action action = () => this.ConsoleLogBox.AppendText(DateTime.Now.ToString("hh:mm:ss.fff") + "[" + logger.GetLoggerName() + "] "+txt+"\n");
            this.Invoke(action);

            //Console.WriteLine(DateTime.Now.ToString("hh:mm:ss.fff") + "[" + logger.GetLoggerName() + "] " + txt);
        }

        public Manufactory Manufactory() {
            return Program.manufactory;
        }             

        private void ButtonConnectClick(object sender, EventArgs e) {
            if(comboPortSelection.SelectedIndex != -1) {
                buttonConnect.Enabled = false;
                buttonDisconnect.Enabled = true;
                Manufactory().OpenSerialPort(comboPortSelection.Text, comboBaudSelection.Text);
                Manufactory().StartModbusSlaveListener();                
            }             
        }

        private void buttonDisconnectClick(object sender, EventArgs e) {
            buttonDisconnect.Enabled = false;
            Manufactory().CloseSerialPort();                        
            buttonConnect.Enabled = true;            
        }

        private void InitializeAnimationForm() {            
            this.animationForm = new AnimationForm();
            this.animationForm.Size = new Size(800, 600);
            this.animationForm.Location = new Point(Width - 850, Location.Y + 40);
            
            this.animationForm.Show(this);           
        }

        private void loadCookiesForm(object sender, EventArgs e) {
            loadConsoleLogBox();
            loadSerialPorts();
            InitializeAnimationForm();
        }        

        private void loadConsoleLogBox() {
            this.consoleWriter = new TextBoxStreamWriter(ConsoleLogBox);
            Console.SetOut(this.consoleWriter);
        }

        private void loadSerialPorts() {
            foreach (string s in System.IO.Ports.SerialPort.GetPortNames()) {
                comboPortSelection.Items.Add(s);
            }
        }              

        private void CookiesForm_Paint(object sender, PaintEventArgs e) {
            Graphics g = this.CreateGraphics();
        }

        private void bStartManufactoryClick(object sender, EventArgs e) {
            this.bStartManufactory.Enabled = false;            
            Manufactory().Start();                        
        }

        private void bStopManufactoryClick(object sender, EventArgs e) {
            Manufactory().Stop();
            this.bStartManufactory.Enabled = true;
            this.bStartForModbus.Enabled = true;            
        }

        private void bStartForModbusClick(object sender, EventArgs e) {
            this.bStartForModbus.Enabled = false;
            this.bStartManufactory.Enabled = false;                    
             
            Manufactory().StartForModbus();

            this.bStopForModbus.Enabled = true;
        }

        private void bStopForModbusClick(object sender, EventArgs e) {
            this.bStopForModbus.Enabled = false;

            Manufactory().StopForModbus();

            this.bStartForModbus.Enabled = true;
            this.bStartManufactory.Enabled = true;            
        }



        public string GetLoggerName() {
            return "MainWindow";
        }

        private AnimationForm animationForm;
        private TextWriter consoleWriter;        

        internal AnimationForm GetAnimationForm() {
            return this.animationForm;
        }
    }
}
