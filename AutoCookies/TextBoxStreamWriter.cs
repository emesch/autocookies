﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoCookies {

    public class TextBoxStreamWriter : TextWriter {

        TextBox output;

        public TextBoxStreamWriter(TextBox output) {
            this.output = output; 
        }

        public override void Write(char value) {
            base.Write(value);
            this.output.AppendText(value.ToString());
        }

        public override Encoding Encoding {
            get {
                return System.Text.Encoding.UTF8;
            }
        }
    }
}
